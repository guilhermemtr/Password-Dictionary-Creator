#include <stdio.h>
#include <math.h> 

int
main(int argc, char ** argv)
{
  int uppercase = 0;
  if (argc == 2) {
    if(argv[1] == "-u")
      uppercase = 1;
    if(argv[1] == "-h")
      printf("usage: ./%s [-h|-u]\n");
  }
  unsigned long long sz = (unsigned long long) pow(16.0, 10.0);
  unsigned long long i = 0;
  if(uppercase) {
    for(i = 0; i < sz; i++) {
      printf("%010X\n", i);
    }
  } else {
    for(i = 0; i < sz; i++) {
      printf("%010x\n", i);
    }
  }

  return 0;
}
